/**
 * @file    finite_state_machine.h
 * @author  Ethan Eymontt
 * @date    12 August 2022
 */

#ifndef SPECTRE_FSM_H
#define SPECTRE_FSM_H

#include <stdint.h>

#ifndef Arduino
    #include <ostream>
#endif

namespace spectre {

/* Type aliasing to enforce API */

using StateIndex        = uint8_t;
using ActionIndex       = uint8_t;
using TransitionIndex   = uint8_t;

const StateIndex unregistered_index = 255;

// function pointers for callbacks

using action_cb     = void (*)();
using condition_cb  = bool (*)();

/** An action manipulates a flag in a specified manner when active. */
struct Action
{
    using ActionType = uint8_t; // underlying type for `FSM::Qualifier` enum

    // @url https://lab4sys.com/en/grafcet-the-basics/
    /** Action qualifier type (see: `Action`). */
    enum class Qualifier : ActionType
    {
        N,  /**< Non-stored. */
            //   variable is held true while assoc. state is on-going

        C,  /**< Conditional. */
            //   variable active when condition is true and state is active

        S,  /**< Set/stored. */
            //   variable is activated when state input is first activated

        R,  /**< Reset. */
            //   variable is reset when state input is first activated

        L,  /**< Time-limited. */
            //   variable is active until timer elapses or state is deactivated; otherwise forced false

        D,  /**< Time-delayed. */
            //   variable is activated when timer elapses up until state is deactivated; otherwise, forced false
    };

    explicit Action(StateIndex inputState, Qualifier type, bool &target, uint32_t delayTime=0, bool *cond=nullptr);

    /**
     * @brief Execute the action.
     * @param[in] timeOnState Duration of activity for current state, in [ms].
     * @param[in] onStateExit Whether the action is being called during a state exit.
     * @note This function is to be called when the current state is its active state.
     */
    void execute(uint32_t timeOnState, bool onStateExit=false);

    /** Qualifier { N, S, R, L, D} that this action applies to. */
    Qualifier type;

    StateIndex state_index;           /**< State during which action will execute. */
    int32_t time_prev = -1;     /**< Time of last execution (-1 = uncalled). */
    uint32_t delay;             /**< Duration of delay for use in L and D actions. */

    bool *target;               /**< Pointer to affected variable. */
    bool *condition;            /**< Pointer to some conditional variable. */

    /** The proceeding action in the linked list storage of action objects. */
    Action *nextAction = nullptr;

    ~Action();
};

/** A representation of a connection between two states. */
struct Transition
{
    explicit Transition(StateIndex inputState, StateIndex outputState, action_cb onXfer=nullptr);
    explicit Transition(StateIndex inputState, StateIndex outputState, condition_cb condition, action_cb onXfer=nullptr);
    explicit Transition(StateIndex inputState, StateIndex outputState, bool &condition, action_cb onXfer=nullptr);

    StateIndex inputState;      /**< State you transition from. */
    StateIndex outputState;     /**< State you transition to. */
    condition_cb condition{};   /**< Trigger the transition based on fn result. */

    bool isActive = false;      /**< True when this instance triggers a transition. */
    action_cb onTransition;     /**< Fn called while transitioning. */

    /** Trigger the transition based on the value of a flag. */
    bool *conditionVar{};

    /** The proceeding transition in the linked list storage of transition objects. */
    Transition *nextTransition{};

    ~Transition();
};

inline bool operator==(const Transition& lhs, const Transition& rhs)
{
    return lhs.inputState == rhs.inputState;
    return lhs.outputState == rhs.outputState;
}
inline bool operator!=(const Transition& lhs, const Transition& rhs){return !operator==(lhs,rhs);}

/** This object contains information for a specific machine state. */
struct State
{
    explicit State(const char *name, action_cb onState);
    explicit State(const char *name, action_cb onEntering, action_cb onState, action_cb onLeaving, uint32_t maxTime=0, uint32_t minTime=0);

    inline void enter() { if (onEntry) onEntry(); };
    inline void run()   { if (onState) onState(); };
    inline void exit()  { if (onLeave) onLeave(); };

    /** Unique index of the state; used for lookup by FSM. */
    StateIndex index{unregistered_index};

    bool timeout = false;   /**< Indicates whether state has "timed out" yet. */
    uint32_t maxTime = 0;   /**< Max duration (0 = no timeout). */
    uint32_t minTime = 0;   /**< Min duration (0 = no minimum). */

    uint32_t enterTime;     /**< Time when state was entered. */

    action_cb onEntry;      /**< Fn called when entering the state. */
    action_cb onLeave;      /**< Fn called when leaving the state. */
    action_cb onState{};    /**< Fn called when state is active. */

    /** Name of the state. */
    const char *stateName;

    /** Pointer to next state in a linked list of defined states. */         
    State *nextState = nullptr;

    /** Pointer to latest action defined to be active during this state. */
    //Action *lastAction = nullptr;

    ~State();
};

inline bool operator==(const State& lhs, const State& rhs)
{
    return lhs.index == rhs.index;
}
inline bool operator!=(const State& lhs, const State& rhs){return !operator==(lhs,rhs);}
inline bool operator< (const State& lhs, const State& rhs)
{
    return lhs.index < rhs.index;
}
inline bool operator> (const State& lhs, const State& rhs){return  operator< (rhs,lhs);}
inline bool operator<=(const State& lhs, const State& rhs){return !operator> (lhs,rhs);}
inline bool operator>=(const State& lhs, const State& rhs){return !operator< (lhs,rhs);}

/** Finite state machine based on actions, states, and transitions. */
class FSM
{
public:
    // constructor
    FSM() = default;

    /* Registration */

    bool registerState(State &state);
    bool registerTransition(Transition &transition);
    bool registerAction(Action &action);

    /** Assign current state to provided index. */
    bool setCurrentStateToIndex(StateIndex index, bool callOnEntering=false);
    
    /** @return Returns index of active state. */
    StateIndex stateIndex() const;

    uint32_t getEnteringTime(StateIndex index);
    uint32_t getDurOfCurrentState();

    void setMaxTimeout(StateIndex index, uint32_t time_ms);
    bool timeout(StateIndex index);
    
    /**
     * @brief Update the FSM.
     * 
     * This is the primary means to operate the FSM after creating it. Call
     * this function every loop.
     * 
     * @param[in] time_curr Current time in [ms].
     * @returns True if the machine transitioned states.
     */
    bool update(uint32_t time_curr);

    bool triggerTransitionAtIndex(TransitionIndex index, bool ignoreSource=false);
    bool triggerTransition(Transition *transition, bool ignoreSource=false);

    inline State *currentState() { return state_curr_; }
    State *getStateAtIndex(StateIndex index);

    inline const char *activeStateName() const
    {
        return state_curr_->stateName;
    }

    inline StateIndex getNumOfStates() const
    {
        return (!state_head_) ? 0 : state_tail_index_ + 1;
    }

private:
    /** Index of last registered state. */
    StateIndex state_tail_index_{0};

    // State references

    State *state_head_{};
    //State *state_tail_{};
    State *state_curr_{};

    /** Index of last registered transition. */
    TransitionIndex trans_tail_index_{0};

    // Transition references

    Transition *trans_head_{};
    //Transition *trans_tail_{};

    /** Index of last registered action. */
    ActionIndex action_tail_index_{0};

    // Action references

    Action *action_head_{};
    //Action *action_tail_{};
    
    uint32_t time_curr_{0}; // milliseconds

    /**
     * @brief Execute an action.
     * @param[in] state Reference to currently-active state.
     * @param[in] action Reference to action.
     * @param[in] onStateExit Whether the action is being called just prior to a state exit.
     */
    void execute_action_(State *state, Action *action, bool onStateExit=false);    
};

#ifndef Arduino
    std::ostream &operator<<(std::ostream &os, Action const &a);
    std::ostream &operator<<(std::ostream &os, Transition const &t);
    std::ostream &operator<<(std::ostream &os, State const &s);
#endif

// identity check
template <typename FSM_Element>
bool areSame(FSM_Element const &a1, FSM_Element const &a2);

} // namespace spectre

#endif /* SPECTRE_FSM_H */