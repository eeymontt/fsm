#ifndef Arduino
    #include <ostream>
#endif

#include "finite_state_machine.h"

namespace spectre {

// delegation constructor
State::State(const char *name, action_cb onState)
    : State(name, nullptr, onState, nullptr)
{}

State::State(const char *name, action_cb onEntering, action_cb onState, action_cb onLeaving, uint32_t maxTime, uint32_t minTime)
    : stateName(name)
    , onEntry(onEntering), onState(onState), onLeave(onLeaving)
    , maxTime(maxTime), minTime(minTime)
{}

State::~State(){}

Action::Action(StateIndex inputState, Qualifier type, bool &target, uint32_t delayTime, bool* cond)
    : state_index(inputState)
    , type(type)
    , target(&target)
    , delay(delayTime)
    , condition(cond)
{}

Action::~Action(){}

void Action::execute(uint32_t timeOnState, bool onStateExit)
{
    bool onStateEnter = !timeOnState;

    /**
     * At the moment:
     *  - Instant states -> target is unaltered (higher precedence than below)
     *  - OnExit -> target always low unless otherwise states
    */
    // instantaneous state changes
    if (onStateEnter && onStateExit) return;

    switch (type)
    {
        case Qualifier::N:
        {
            *target = !onStateExit;
            break;
        }
        case Qualifier::C:
        {
            *target = (condition && *condition);
            break;
        }
        case Qualifier::S:
        {
            if (onStateEnter) *target = true;
            break;
        }
        case Qualifier::R:
        {
            if (onStateEnter) *target = false;
            break;
        }
        case Qualifier::L:
        {
            *target = (timeOnState < delay) ? true : false;
            *target &= (condition && *condition);   // set low if condition is assigned but false
            if (onStateExit) *target = false;
            break;
        }
        case Qualifier::D:
        {
            *target = true;
            if ((timeOnState < delay) || onStateExit) *target = false;
            *target &= (condition && *condition);   // set low if condition is assigned but false
            break;
        } 
    }
    if (onStateExit) *target = false;
}

Transition::Transition(StateIndex inputState, StateIndex outputState, action_cb onXfer)
    : inputState(inputState)
    , outputState(outputState)
    , onTransition(onXfer)
{}

// delegation constructor
Transition::Transition(StateIndex inputState, StateIndex outputState, condition_cb condition, action_cb onXfer)
    : Transition(inputState, outputState, onXfer)
{
    this->condition = condition;
}

// delegation constructor
Transition::Transition(StateIndex inputState, StateIndex outputState, bool &condition, action_cb onXfer)
    : Transition(inputState, outputState, onXfer)
{
    this->conditionVar = &condition;
}

Transition::~Transition(){}

/* Client API functions. */

bool FSM::registerState(State &state)
{
    if (state.index != unregistered_index)
    {
        // state already registered
        return false;
    }

    if (!state_head_) // no registered states yet
    {
        state_head_ = &state;
        state.index = 0;
    }
    else
    {
        State *state_tail = getStateAtIndex(state_tail_index_);
        state_tail->nextState = &state;
        state.index = ++state_tail_index_;
    }
    return true;
}

bool FSM::registerTransition(Transition &transition)
{
    if (!trans_head_) // no registered transitions yet
    {
        trans_head_ = &transition;
    }
    else
    {
        Transition *trans_tail;
        for (Transition *t = trans_head_; t; t = t->nextTransition)
        {
            trans_tail = t;

            // make sure transition isn't already registered by this FSM
            if (areSame(&transition, t)) return false;
        }
        trans_tail->nextTransition = &transition;
    }
    trans_tail_index_++;
    return true;
}

bool FSM::registerAction(Action &action)
{
    if (!action_head_) // no registered actions yet
    {
        action_head_ = &action;
    }
    else
    {
        Action *action_tail;
        for (Action *t = action_head_; t; t = t->nextAction)
        {
            action_tail = t;

            // make sure action isn't already registered by this FSM
            if (areSame(&action, t)) return false;
        }
        action_tail->nextAction = &action;
    }
    action_tail_index_++;
    return true;
}

uint32_t FSM::getDurOfCurrentState()
{
    if (!state_curr_) return 0;
    return time_curr_ - state_curr_->enterTime;
}

bool FSM::triggerTransitionAtIndex(TransitionIndex index, bool ignoreSource)
{
    if (!state_curr_) return false;
    if (!trans_head_) return false; /* No registered transitions. */

    int idx = 0;
    for (Transition *t = trans_head_; t; t = t->nextTransition)
    {
        if (index == idx)
        {
            return triggerTransition(t, ignoreSource);
        }
        idx++; 
    }

    return false;   /* No transition with matching index. */
}

bool FSM::triggerTransition(Transition *transition, bool ignoreSource)
{
    if (!state_curr_) return false;
    if ((transition->inputState == state_curr_->index) || ignoreSource)
    {
        // run "on exit" callback for the exiting state
        state_curr_->exit();

        /* Does the exiting state have any actions that need to be
        checked on before the state can leave? */
        for (Action *a = action_head_; a; a = a->nextAction)
        {
            if (a->state_index == state_curr_->index)
            {
                a->execute(getDurOfCurrentState(), true);
            }
        }

        if (transition->onTransition) transition->onTransition();
        return setCurrentStateToIndex(transition->outputState, true);
    }
    return false;
}

bool FSM::update(uint32_t time_curr)
{
    // machine needs to be running
    if(!state_curr_) return false;

    // update machine time
    if (time_curr > time_curr_) time_curr_ = time_curr;
    const uint32_t time_dur = getDurOfCurrentState();

    // determine whether state has "timed out"
    // don't time out if min time not met
    state_curr_->timeout = (state_curr_->maxTime && (state_curr_->maxTime > time_dur));
    if (state_curr_->minTime > time_dur) state_curr_->timeout = false;

    // check for applicable transitions
    Transition *t;
    for (t = trans_head_; t; t = t->nextTransition)
    {
        if (t->inputState == state_curr_->index)
        {
            if (t->condition && t->condition()) break;
            if (t->conditionVar && *(t->conditionVar)) break;
            if (state_curr_->timeout) break;
        }
    }

    // run "on state" if no transitions found
    if (!t)
    {
        state_curr_->run();
        return false;
    }

    // first valid transition
    return triggerTransition(t, true);
}

State *FSM::getStateAtIndex(StateIndex index)
{
    // iterate through list of states until the next state is a `nullptr`
    for (State *s = state_head_; s; s = s->nextState)
    {
        if (s->index == index) return s;
    }
    return nullptr; // no state associated with provided index
}

StateIndex FSM::stateIndex() const
{
    if (state_curr_) return state_curr_->index;
    return unregistered_index;
}

bool FSM::setCurrentStateToIndex(StateIndex index, bool callOnEntering)
{
    State *newState = getStateAtIndex(index);

    if (!newState) return false;

    state_curr_            = newState;
    state_curr_->enterTime = time_curr_;
    state_curr_->timeout   = false;

    if (callOnEntering) { state_curr_->enter(); }

    for (Action *a = action_head_; a; a = a->nextAction)
    {
        if (a->state_index == state_curr_->index) {
            a->execute(0, false);
        }
    }
    return true;
}

void FSM::setMaxTimeout(StateIndex index, uint32_t time_ms)
{
    State *state = getStateAtIndex(index);
    if (state) state->maxTime = time_ms;
}

bool FSM::timeout(StateIndex index)
{
    State *state = getStateAtIndex(index);
    return (state ? state->timeout : false);
}

uint32_t FSM::getEnteringTime(StateIndex index)
{
    State *state = getStateAtIndex(index);
    return (state ? state->enterTime : 0);
}

#ifndef Arduino
/* Stream insertion overloading for non-Arduino. */

    std::ostream &operator<<(std::ostream &os, Action const &a)
    {
        os << "[A: id=" << static_cast<int>(a.type);
        os << ", index=" << static_cast<int>(a.state_index);
        os << ", delay=" << a.delay << "]";
        return os;
    }

    std::ostream &operator<<(std::ostream &os, Transition const &t)
    {
        os << "[T: " << static_cast<int>(t.inputState);
        os << " -> " << static_cast<int>(t.outputState) << "]";
        return os;
    }

    std::ostream &operator<<(std::ostream &os, State const &s)
    {
        os << "State{name=\"" << s.stateName;
        os << "\", index=" << static_cast<int>(s.index);
        os << "\", minTime=" << s.minTime;
        os << ", maxTime=" << s.maxTime << "}";
        return os;
    }

#endif /* Arduino */

/* Helpful functions. */

template <typename FSM_Element>
bool areSame(FSM_Element const &a1, FSM_Element const &a2)
{
    return &a1 == &a2;
}

} // namespace spectre
