#include "gtest/gtest.h"
#include "gmock/gmock.h"
#include "finite_state_machine.h"

#include <tuple>

//using ::testing::Address;
using ::testing::Pointee;

TEST(SimpleTransitions, OnCreateNoTransitionsAdded)
{
    spectre::FSM fsm;
    EXPECT_FALSE(fsm.triggerTransitionAtIndex(0));
}

TEST(SimpleTransitions, TransitionCallsRequireAStateSet)
{
    spectre::FSM fsm;
    spectre::Transition s0_to_s0 = spectre::Transition(0, 0);
    fsm.registerTransition(s0_to_s0);
    ASSERT_FALSE(fsm.triggerTransitionAtIndex(0));
    auto s = spectre::State("State 0", nullptr);
    fsm.registerState(s);
    
    fsm.setCurrentStateToIndex(0);  // <--

    EXPECT_TRUE(fsm.triggerTransitionAtIndex(0));
}

class FsmWithThreeSimpleTransitions : public ::testing::Test
{
    protected:
        FsmWithThreeSimpleTransitions()
        {
            fsm.registerState(state0);
            fsm.registerState(state1);
            fsm.registerState(state2);
            fsm.registerTransition(s0_to_s1);
            fsm.registerTransition(s1_to_s2);
            fsm.registerTransition(s2_to_s0);
            fsm.setCurrentStateToIndex(0);
        }

        spectre::FSM fsm;
        spectre::State state0 = spectre::State("State 0", nullptr);
        spectre::State state1 = spectre::State("State 1", nullptr);
        spectre::State state2 = spectre::State("State 2", nullptr);
        spectre::Transition s0_to_s1 = spectre::Transition(0, 1);
        spectre::Transition s1_to_s2 = spectre::Transition(1, 2);
        spectre::Transition s2_to_s0 = spectre::Transition(2, 0);
};

TEST_F(FsmWithThreeSimpleTransitions, OutOfBoundsTransCallIsFalse)
{
    EXPECT_FALSE(fsm.triggerTransitionAtIndex(5));
}

TEST_F(FsmWithThreeSimpleTransitions, ItWorksWithOne)
{
    fsm.registerTransition(s0_to_s1);
    fsm.setCurrentStateToIndex(0);
    EXPECT_TRUE(fsm.triggerTransitionAtIndex(0));
}

TEST_F(FsmWithThreeSimpleTransitions, TransitionsAddedInOrder)
{
    EXPECT_THAT(s0_to_s1.nextTransition, Pointee(s1_to_s2));
    EXPECT_THAT(s1_to_s2.nextTransition, Pointee(s2_to_s0));
    EXPECT_EQ(s2_to_s0.nextTransition, nullptr);
}

TEST_F(FsmWithThreeSimpleTransitions, TriggerTransitionByIndexWhenValid)
{
    EXPECT_TRUE(fsm.triggerTransitionAtIndex(0));
    EXPECT_THAT(fsm.currentState(), Pointee(state1));

    EXPECT_TRUE(fsm.triggerTransitionAtIndex(1));
    EXPECT_THAT(fsm.currentState(), Pointee(state2));

    EXPECT_TRUE(fsm.triggerTransitionAtIndex(2));
    EXPECT_THAT(fsm.currentState(), Pointee(state0));
}

TEST_F(FsmWithThreeSimpleTransitions, TriggerTransitionByIndexWhenInvalid)
{
    EXPECT_FALSE(fsm.triggerTransitionAtIndex(1));
    EXPECT_THAT(fsm.currentState(), Pointee(state0));
}

TEST_F(FsmWithThreeSimpleTransitions, TriggerTransitionByIndexNotRegistered)
{
    EXPECT_FALSE(fsm.triggerTransitionAtIndex(4));
    EXPECT_THAT(fsm.currentState(), Pointee(state0));
}

TEST_F(FsmWithThreeSimpleTransitions, TriggerTransitionByIndexWhenForced)
{
    EXPECT_TRUE(fsm.triggerTransitionAtIndex(1, true));
    EXPECT_EQ(fsm.currentState()->index, s1_to_s2.outputState);

    EXPECT_TRUE(fsm.triggerTransitionAtIndex(0, true));
    EXPECT_EQ(fsm.currentState()->index, s0_to_s1.outputState);

    EXPECT_TRUE(fsm.triggerTransitionAtIndex(2, true));
    EXPECT_EQ(fsm.currentState()->index, s2_to_s0.outputState);

    EXPECT_TRUE(fsm.triggerTransitionAtIndex(2, true));
    EXPECT_EQ(fsm.currentState()->index, s2_to_s0.outputState);
}