#include "gtest/gtest.h"
#include "finite_state_machine.h"

#include <tuple>

class ActionQualifierN : public ::testing::Test
{
    protected:
        bool target{false};
        spectre::Action action_N = spectre::Action(0, spectre::Action::Qualifier::N, target);
};

TEST_F(ActionQualifierN, TrueOnStartupFromFalse)
{
    ASSERT_FALSE(target);
    action_N.execute(0);
    EXPECT_TRUE(target);
}

TEST_F(ActionQualifierN, TrueOnStartupFromTrue)
{
    target = true;
    ASSERT_TRUE(target);
    action_N.execute(0);
    EXPECT_TRUE(target);
}

TEST_F(ActionQualifierN, FalseOnExitFromFalse)
{
    ASSERT_FALSE(target);
    action_N.execute(50, true);
    EXPECT_FALSE(target);
}

TEST_F(ActionQualifierN, FalseOnExitFromTrue)
{
    target = true;
    ASSERT_TRUE(target);
    action_N.execute(50, true);
    EXPECT_FALSE(target);
}

TEST_F(ActionQualifierN, FalseOnExitWithInstantTransitionFromFalse)
{
    ASSERT_FALSE(target);
    action_N.execute(0, true);
    EXPECT_FALSE(target);
}

TEST_F(ActionQualifierN, TrueOnExitWithInstantTransitionFromTrue)
{
    target = true;
    ASSERT_TRUE(target);
    action_N.execute(0, true);
    EXPECT_TRUE(target);
}

TEST_F(ActionQualifierN, TrueAtAllAnyTimeDuring)
{
    ASSERT_FALSE(target);
    action_N.execute(50);
    EXPECT_TRUE(target);
}

class CParametersTests:
    public ::testing::TestWithParam<std::tuple<bool, bool, bool>>
{
    protected:
        bool target;
        bool condition;

        spectre::Action action_C = spectre::Action(
            0,
            spectre::Action::Qualifier::C,
            target,
            0,
            &condition
        );

    public:
        struct PrintToStringParamName
        {
            template <class ParamType>
            std::string operator()(const testing::TestParamInfo<ParamType>& info) const
            {
                std::string param0 = std::get<0>(info.param) ? "True" : "False";
                std::string param1 = std::get<1>(info.param) ? "True" : "False";
                std::string param2 = std::get<2>(info.param) ? "True" : "False";
                return "When" + param0 + "AndStarted" + param1 + "Be" + param2;
            }
        };
};

TEST_P(CParametersTests, OnStateStartup)
{
    bool expected = std::get<2>(GetParam());

    target = std::get<1>(GetParam());
    condition = std::get<0>(GetParam());
    action_C.execute(0);
    EXPECT_EQ(expected, target);
}

TEST_P(CParametersTests, OnStateExit)
{
    target = std::get<1>(GetParam());
    condition = std::get<0>(GetParam());
    action_C.execute(50, true);
    EXPECT_FALSE(target);
}

TEST_P(CParametersTests, InstantaneousDuration)
{
    target = std::get<1>(GetParam());
    bool original = std::get<1>(GetParam());
    action_C.execute(0, true);
    EXPECT_EQ(target, original);
}

INSTANTIATE_TEST_SUITE_P(
    ActionQualifierC,
    CParametersTests,
    ::testing::Values(
        /* condition, target start value, expected */
        std::make_tuple(false, false, false),
        std::make_tuple(false, true,  false),
        std::make_tuple(true,  false, true),
        std::make_tuple(true,  true,  true)
    ),
    CParametersTests::PrintToStringParamName()
);

class ActionQualifierS : public ::testing::Test
{
    protected:
        bool target{false};
        spectre::Action action_S = spectre::Action(0, spectre::Action::Qualifier::S, target);
};

class ActionQualifierR : public ::testing::Test
{
    protected:
        bool target{false};
        spectre::Action action_R = spectre::Action(0, spectre::Action::Qualifier::R, target);
};

class ActionQualifierL : public ::testing::Test
{
    protected:
        bool target{false};
        bool condition{false};

        spectre::Action action_L = spectre::Action(
            0,
            spectre::Action::Qualifier::L,
            target,
            0,
            &condition
        );

        spectre::Action action_L_defaults = spectre::Action(
            0,
            spectre::Action::Qualifier::L,
            target
        );
};

class ActionQualifierD : public ::testing::Test
{
    protected:
        bool target{false};
        bool condition{false};
        spectre::Action action_D = spectre::Action(
            0,
            spectre::Action::Qualifier::D,
            target,
            0,
            &condition
        );

        spectre::Action action_D_defaults = spectre::Action(
            0,
            spectre::Action::Qualifier::D,
            target
        );
};