#include "gtest/gtest.h"
#include "finite_state_machine.h"

#include <tuple>

TEST(StateConstructors, NameIsAsProvided)
{
    spectre::State state0 = spectre::State("State 0", nullptr);
    EXPECT_STREQ(state0.stateName, "State 0");
}

TEST(StateConstructors, NameIsAsProvided1)
{
    spectre::State state0 = spectre::State("State 0", nullptr);
    EXPECT_STREQ(state0.stateName, "State 0");
}

TEST(StateConstructors, NameIsAsProvided2)
{
    spectre::State state0 = spectre::State("State 0", nullptr);
    EXPECT_STREQ(state0.stateName, "State 0");
}

TEST(StateConstructors, NameIsAsProvided3)
{
    spectre::State state0 = spectre::State("State 0", nullptr);
    EXPECT_STREQ(state0.stateName, "State 0");
}

TEST(StateConstructors, NameIsAsProvided4)
{
    spectre::State state0 = spectre::State("State 0", nullptr);
    EXPECT_STREQ(state0.stateName, "State 0");
}

class SingleStateProperties : public ::testing::Test
{
    protected:
        SingleStateProperties()
        {
            fsm.registerState(state0);
        }

        spectre::FSM fsm;
        spectre::State state0 = spectre::State("State 0", nullptr);
};

TEST_F(SingleStateProperties, CountOfOneState)
{
    EXPECT_EQ(fsm.getNumOfStates(), 1);
}

TEST_F(SingleStateProperties, CurrentStateNotAssignedByDefault)
{
    EXPECT_EQ(fsm.currentState(), nullptr);
}

TEST_F(SingleStateProperties, RegisteredStateIndexStartsAtZero)
{
    EXPECT_EQ(fsm.getStateAtIndex(0), &state0);
    EXPECT_EQ(state0.index, 0);
}

TEST_F(SingleStateProperties, CannotReregisterAState)
{
    fsm.registerState(state0);
    EXPECT_EQ(fsm.getNumOfStates(), 1);
    EXPECT_EQ(state0.index, 0);
}

TEST_F(SingleStateProperties, SetCurrentStateToValid)
{
    EXPECT_TRUE(fsm.setCurrentStateToIndex(0));
    EXPECT_EQ(fsm.currentState(), &state0);
    EXPECT_EQ(fsm.getStateAtIndex(0), &state0);
}

TEST_F(SingleStateProperties, SetCurrentStateToInvalidIsFalse)
{
    EXPECT_FALSE(fsm.setCurrentStateToIndex(2));
}

TEST_F(SingleStateProperties, SetCurrentStateToInvalidDoesNotChangeState)
{
    fsm.setCurrentStateToIndex(0);
    ASSERT_EQ(fsm.currentState(), &state0);

    EXPECT_FALSE(fsm.setCurrentStateToIndex(2));

    EXPECT_EQ(fsm.currentState(), &state0);
}