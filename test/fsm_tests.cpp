#include "gtest/gtest.h"
#include "gmock/gmock.h"
#include "finite_state_machine.h"

#include <tuple>

//using ::testing::Address;
using ::testing::Pointee;

class DefaultFsmProperties : public ::testing::Test
{
    protected:
        spectre::FSM fsm;
};

TEST_F(DefaultFsmProperties, NoStatesRegistered)
{
    EXPECT_EQ(fsm.getNumOfStates(), 0);
}

TEST_F(DefaultFsmProperties, StateTimingsAreZero)
{
    EXPECT_EQ(fsm.getDurOfCurrentState(), 0);
}

TEST_F(DefaultFsmProperties, CurrentStateIsNotTimedOut)
{
    EXPECT_FALSE(fsm.timeout(0));
}

TEST_F(DefaultFsmProperties, StateEnteringTimeIsZero)
{
    EXPECT_EQ(fsm.getEnteringTime(0), 0);
}

TEST_F(DefaultFsmProperties, CurrentStateIsNull)
{
    EXPECT_EQ(fsm.currentState(), nullptr);
}

TEST_F(DefaultFsmProperties, AllStateIndicesAreNull)
{
    EXPECT_EQ(fsm.getStateAtIndex(0), nullptr);
    EXPECT_EQ(fsm.getStateAtIndex(1), nullptr);
    EXPECT_EQ(fsm.getStateAtIndex(255), nullptr);
}

TEST_F(DefaultFsmProperties, UnregisteredTransIndexDoesNotCrash)
{
    EXPECT_FALSE(fsm.triggerTransitionAtIndex(0));
}

class FsmWithThreeStates : public ::testing::Test
{
    protected:
        FsmWithThreeStates()
        {
            fsm.registerState(state0);
            fsm.registerState(state1);
            fsm.registerState(state2);
        }

        spectre::FSM fsm;
        spectre::State state0 = spectre::State("State 0", nullptr);
        spectre::State state1 = spectre::State("State 1", nullptr);
        spectre::State state2 = spectre::State("State 2", nullptr);
};

TEST_F(FsmWithThreeStates, CountOfStateIsSameAsRegistered)
{
    EXPECT_EQ(fsm.getNumOfStates(), 3);
}

TEST_F(FsmWithThreeStates, CurrentStateNotAssignedAutomatically)
{
    EXPECT_EQ(fsm.currentState(), nullptr);
}

TEST_F(FsmWithThreeStates, AddingNewStateDoesNotChangeCurrentState)
{
    fsm.setCurrentStateToIndex(2);
    auto s_start = fsm.currentState();

    auto state3 = spectre::State("State 3", nullptr);
    fsm.registerState(state3);

    auto s_end = fsm.currentState();
    EXPECT_STREQ(s_start->stateName, s_end->stateName);
}

TEST_F(FsmWithThreeStates, CannotSetStateWithInvalidIndex)
{
    ASSERT_EQ(fsm.currentState(), nullptr);
    fsm.setCurrentStateToIndex(20);
    EXPECT_EQ(fsm.currentState(), nullptr);
}

TEST_F(FsmWithThreeStates, CanSetStateWithValidIndexWithNoStateSetToFirst)
{
    ASSERT_EQ(fsm.currentState(), nullptr);
    fsm.setCurrentStateToIndex(0);
    EXPECT_THAT(fsm.currentState(), Pointee(state0));
}

TEST_F(FsmWithThreeStates, CanSetStateWithValidIndexWithNoStateSetToNotFirst)
{
    ASSERT_EQ(fsm.currentState(), nullptr);
    fsm.setCurrentStateToIndex(1);
    EXPECT_THAT(fsm.currentState(), Pointee(state1));
}

TEST_F(FsmWithThreeStates, CanSetStateWithValidIndexWithStateSet)
{
    fsm.setCurrentStateToIndex(0);
    ASSERT_THAT(fsm.currentState(), Pointee(state0));
    fsm.setCurrentStateToIndex(1);
    EXPECT_THAT(fsm.currentState(), Pointee(state1));
}

TEST(TriggerTransition, ByTransitionReference)
{
    spectre::FSM fsm;
    auto state0 = spectre::State("State 0", nullptr);
    auto state1 = spectre::State("State 1", nullptr);
    auto s0_to_s1 = spectre::Transition(0, 1);
    fsm.registerState(state0);
    fsm.registerState(state1);
    fsm.registerTransition(s0_to_s1);
    fsm.setCurrentStateToIndex(0);
    EXPECT_TRUE(fsm.triggerTransition(&s0_to_s1));
}