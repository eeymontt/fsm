/**
 * @file    turnstile.cpp
 * @brief   Example of a turnstile mechanism.
 * @author  Ethan Eymontt
 * @date    03 Sept 2022
 */

#include <iostream>
#include <stdint.h>
#include <chrono>
    
#include "finite_state_machine.h"

// forward declarations
static uint16_t getTimeInMs(); // get current (relative) time

char c;

using std::cin, std::cout, std::endl;

int main(int argc, char* argv[])
{
    spectre::FSM m;

    spectre::State s1("S1",
        [](){cout << "  ... -> S1" << endl;},
        [](){cout << "  (S1)" << endl;},
        [](){cout << "  S1 -> ..." << endl;}
    );
    spectre::State s2("S2",
        [](){cout << "  ... -> S2" << endl;},
        [](){cout << "  (S2)" << endl;},
        [](){cout << "  S2 -> ..." << endl;}
    );

    m.registerState(s1);
    m.registerState(s2);

    spectre::Transition s1_to_s2(
        s1.index, s2.index,
        [](){return c == '2';}, [](){cout << "  S1 >>> S2 " << endl;});
    spectre::Transition s2_to_s1(
        s2.index, s1.index,
        [](){return c == '1';}, [](){cout << "  S2 >>> S1" << endl;});

    /*
    spectre::Transition s1_to_s1(
        s1.index, s1.index,
        [](){return c == '1';}, [](){cout << "  S1 >>> S1" << endl;});
    spectre::Transition s2_to_s2(
        s2.index, s2.index,
        [](){return c == '2';}, [](){cout << "  S2 >>> S2" << endl;});
    */

    //m.registerTransition(&s1_to_s1);
    m.registerTransition(s1_to_s2);
    m.registerTransition(s2_to_s1);
    //m.registerTransition(&s2_to_s2);
    m.setCurrentStateToIndex(s2.index);

    cout << "Enter `q` to quit." << endl;
    cout << "Press `1` to enter State 1 or `2` to enter State 2." << endl;
    bool running = true;
    while(running)
    {
        cout << "\nWhat would you like to do? " << endl;
        //if(cin.peek() == EOF) continue; // no new inputs
        cin.get(c);
        cin.ignore(1);
        cout << "[CMD: " << c << "]" << endl;

        if (c == 'q')
        {
            running = false;
            break;
        }
        //else if (c == 'c') cout << "you enter a coin" << endl;
        //else if (c == 'p') cout << "you push on the tunstile" << endl;
        //else cin.clear();
        
        cout << "UPDATE:START" << endl;
        m.update(getTimeInMs());
        cout << "UPDATE:END" << endl;
        //cout << "state: " << m.activeStateName() << endl;
        //cin.clear();
    }
}

static uint16_t getTimeInMs()
{
    using std::chrono::duration_cast;
    using std::chrono::milliseconds;
    using std::chrono::system_clock;
    return duration_cast<milliseconds>(system_clock::now().time_since_epoch()).count();
}