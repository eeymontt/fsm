/**
 * @file    turnstile.cpp
 * @brief   Example of a turnstile mechanism.
 * @author  Ethan Eymontt
 * @date    03 Sept 2022
 */

#include <iostream>
#include <stdint.h>
#include <chrono>
    
#include "finite_state_machine.h"

// forward declarations
static uint16_t getTimeInMs(); // get current (relative) time
static void whenLocked();
static void whenUnlocked();

char c;

using std::cin, std::cout, std::endl;

int main(int argc, char* argv[])
{
    spectre::FSM m;

    spectre::State s1("locked", nullptr, whenLocked, nullptr);
    spectre::State s2("unlocked", nullptr, whenUnlocked, nullptr);

    m.registerState(s1);
    m.registerState(s2);

    spectre::Transition s1_to_s2(
        s1.index, s2.index,
        [](){return c == 'c';}, [](){cout << "unlocking..." << endl;});
    spectre::Transition s2_to_s1(
        s2.index, s1.index,
        [](){return c == 'p';}, [](){cout << "locking..." << endl;});
    spectre::Transition s1_to_s1(
        s1.index, s1.index,
        [](){return c == 'p';}, [](){cout << "it remains locked" << endl;});
    spectre::Transition s2_to_s2(
        s2.index, s2.index,
        [](){return c == 'c';}, [](){cout << "it remains unlocked" << endl;});

    m.registerTransition(s1_to_s1);
    m.registerTransition(s1_to_s2);
    m.registerTransition(s2_to_s1);
    m.registerTransition(s2_to_s2);
    m.setCurrentStateToIndex(s2.index);

    cout << "Enter `q` to quit." << endl;
    cout << "Press `c` to enter a coin or `p` to push.\n" << endl;
    bool running = true;
    while(running)
    {
        //if(cin.peek() == EOF) continue; // no new inputs
        cin.get(c);
        cin.ignore(1);

        if (c == 'q')
        {
            running = false;
            break;
        }
        else if (c == 'c') cout << "you enter a coin" << endl;
        else if (c == 'p') cout << "you push on the tunstile" << endl;
        //else cin.clear();
        
        m.update(getTimeInMs());
        //cout << "state: " << m.activeStateName() << endl;
        //cin.clear();
    }
}

static uint16_t getTimeInMs()
{
    using std::chrono::duration_cast;
    using std::chrono::milliseconds;
    using std::chrono::system_clock;
    return duration_cast<milliseconds>(system_clock::now().time_since_epoch()).count();
}

static void whenLocked()
{
    cout << "you face a locked turnstile" << endl;
}

static void whenUnlocked()
{
    cout << "you face an unlocked turnstile" << endl;
}