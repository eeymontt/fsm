set(EXAMPLE_MATTER matter)

add_executable(${EXAMPLE_MATTER} states_of_matter.cpp)

target_compile_features(${EXAMPLE_MATTER} PRIVATE cxx_std_17)
target_link_libraries(${EXAMPLE_MATTER} PRIVATE fsm)
