/**
 * @file    states_of_matter.cpp
 * @brief   An example where we explore the states of matter.
 * @author  Ethan Eymontt
 * @date    25 October 2022
 */

#include <iostream>
#include <stdint.h>
#include <chrono>
    
#include "finite_state_machine.h"

using std::cout, std::endl;
int main(int argc, char* argv[])
{
    /* Create Finite State Machine */

    spectre::FSM m;

    spectre::State solid("Solid", nullptr, nullptr, nullptr);
    spectre::State liquid("Liquid", nullptr, nullptr, nullptr);
    spectre::State gas("Gas", nullptr, nullptr, nullptr);
    spectre::State plasma("Plasma", nullptr, nullptr, nullptr);

    m.registerState(&solid);
    m.registerState(&liquid);
    m.registerState(&gas);
    m.registerState(&plasma);

    spectre::Transition melt(
        solid.index, liquid.index,
        [](){return true;},
        [](){cout << "Melting..." << endl;}
    );

    spectre::Transition freeze(
        liquid.index, solid.index,
        [](){return true;},
        nullptr
    );

    spectre::Transition evaporate(
        liquid.index, gas.index,
        [](){return true;},
        nullptr
    );

    spectre::Transition condense(
        gas.index, liquid.index,
        [](){return true;},
        nullptr
    );

    spectre::Transition sublimate(
        solid.index, gas.index,
        [](){return true;},
        nullptr
    );

    spectre::Transition ionize(
        gas.index, plasma.index,
        [](){return true;},
        nullptr
    );

    spectre::Transition deionize(
        plasma.index, gas.index,
        [](){return true;},
        nullptr
    );

    m.registerTransition(&melt);
    m.registerTransition(&freeze);
    m.registerTransition(&evaporate);
    m.registerTransition(&condense);
    m.registerTransition(&sublimate);
    m.registerTransition(&ionize);
    m.registerTransition(&deionize);

    /* Mess with some matter */

    m.setState(solid.index);
    cout << "State: " << m.currentState()->stateName << endl;

    m.triggerTransition(0);
    //m.triggerTransition(melt.index);
    cout << "State: " << m.currentState()->stateName << endl;

}

/*
while(running)
{
    const uint16_t time_current_ms =
        std::chrono::duration_cast<std::chrono::milliseconds>
        (std::chrono::system_clock::now().time_since_epoch()).count();
    
    m.update(time_current_ms);
}
*/