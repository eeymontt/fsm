# Finite State Machine

This library is used to represent a finite state machine (FSM) that incorporates the ideas of:

* State-based actions that can change an external variable as with a [Mealy machine](https://en.wikipedia.org/wiki/Mealy_machine); and
* Entry/exit actions of a [Moore machine](https://en.wikipedia.org/wiki/Moore_machine)

And is thus comparable to a [UML state machine](https://en.wikipedia.org/wiki/UML_state_machine) in that hierarchical states can be represented; but, at least through this library, they are not explicitly interfaceable.

## Methodology

This utility was designed with several goals in mind; the code is expected to be:

* Flexible
* Lightweight
* Automatic
* Extensible

Flexible means that the basic framework can be applied without modification to a variety of tasks with a range of requirements. Not every application needs the same capabilities, and so not every tool provided by the **FSM** library will be used in every case.

The code is written to be lightweight since it is designed to be run on embedded systems (_at least for the author's purposes_). This means that all memory allocation is static and that utilization of platform-specific tools/libraries is removed or extremely limited to when necessary.

What is "automatic" code? This attribute pertains to the models developed with the library themselves, as does "extensible". Ideally, the models written with **FSM** do not require manual triggers. A single `update()` call from the main program loop is all that is needed to operate/advance the model. This is accomplished through actions (see: `Actions`) and callbacks (see: [Callbacks](#callbacks)).

That said, some tools are provided for the developer to manually trigger events and call state functions—but these should mainly be used for debugging.

***

## Namespace

The finite state machine library exists within the `eymonttdev::fsm` namespace.

## States

Machine states are represented by `State` objects. To add a state to an FSM, it must first be created and then registered.

```c++
auto myState = State("StateName", nullptr);
fsm.registerState(&myState);
```

Once registered, a state can now be referenced by its `index`. To ~~transition~~ force the machine to a specific state (whereby it does not execute state edge callbacks):

```c++
fsm.setStateToIndex(myState.index);
```

To see the current state of a machine:

```c++
cout << "State: " << fsm.currentState()->stateName << endl;
```

Although states are not stored within an FSM directly, they should be expected to persist throughout the lifetime of the `FSM` object. There is no way to de-register a state.

Although you probably shouldn't, you can retrieve pointers to states from the FSM. This may be useful for debugging:

```c++
State s = fsm.stateIndex(0);    // retrieve first state registered to the machine
s.onState();                    // execute the on-state method for that state
```

### Times states

States can be provided time parameters for minimum and maximum duration.

_Note: The minimum time will always be observed!_

For instance, if the two parameters create a mutually-exclusive definition (such as a minimum time = 2 seconds, and a maximum time = 1 second), the minimum time takes precedence and the state will last for 2 seconds before transitioning to the next state.

## Callbacks

Callback functions are a major aspect of the **FSM** library; they are what provide functionality and meaning to different states in a model. They can be assigned to states and to transitions and will execute automatically whenever applicable. Callbacks are assigned to objects when those objects are created.

```c++
// create an "empty" State, i.e. one that does not do anything when active
State s1("EmptyState", nullptr);

// this state lets us know when it is running
void onState() {std::cout << "BasicState is active!" << std::endl;}
State s2("BasicState", onState);
```

Callbacks can be assigned by passing a reference to an externally defined function, as shown above, or through lambdas, as shown below.

```c++
State s("S", [](){cout << "Running..." << endl;});

// assign entry/exit callbacks, too
State s3("FunctionalState",
    [](){cout << "On-entry fn" << endl;},
    [](){cout << "During..." << endl;},
    [](){cout << "On-exit fn" << endl;}
);
```

Entry and exit callbacks are assigned to the state itself, so when transitioning between states, they would be called in order of appearance. For example with a transition, a leaving state's on-exit callback would execute first, followed by any callback associated with that transition. Then, if assigned, the new state's on-entry callback would execute. Since a transition occurs during a single `update()` to the model, all of the assigned callbacks will run automatically during one program execution loop.

### Time-based states

_Warning!_ It is the responsibility of the user when forming their state machine to prevent invalid (or "stall" conditions). When providing a max- or min-time parameter to a state, the user must make sure to also provide a valid ["time" transition](#timed-transition) for that state.

You should not have any issues if you adhere to the following:

1) Only one timed transition shall be registered to any single state in a machine; and
1) Every state with a `maxTime` parameter set must have a timed transition registered with the machine.

What this means is that for all timed transitions registered with the machine, no two transitions shall have the same source state index, and that all states that can time out need to have a transition that will be called when they time out.

## Transitions

Transitions bind states together.

Transitions are stored as an ordered list inside of the `FSM` object matching the order in which they were registered... more on this later.

### Create a transition

Transitions require a source state and destination state in order to be defined.

```c++
Transition t(0, 1); // indices of source and dest. states, respectively
```

Note that there is no restriction on the states that a `Transition` accepts, so you can supply the same `State` instance index for the start and end state parameters; this is useful in cases where you have dedicated entry or exit actions that you would like to execute in response to specific inputs. See: [Reflexive Transitions](#reflexive-transitions).

### Timed or not?

There are two main types of transitions:

* Those that are triggered by some state-based timer; and
* Those that are triggered a condition placed on the transition itself.

### Transitions with conditions

Most transitions will have a condition associated with them. These transitions are called _conditional transitions_ and are provided either a reference to a single, external value or a reference to a function that returns `true` when the transition should be triggered.

```c++
bool flag_to_xfer = false;
bool flag_to_xfer2 = false;

// create a transition based on a single variable
Transition s1_to_s2_single(StateIndex(STATE_1), StateIndex(STATE_2), flag_to_xfer);

// create a transition based on a (lambda) function
Transition s1_to_s2_double(StateIndex(STATE_1), StateIndex(STATE_2), [](){return flag_to_xfer || flag_to_xfer2;});
```

_Note: Be careful when assigning transition start/stop indices via `spectre::State.index`._ The `State` index parameter is assigned only when the `FSM` registers it.

During a machine update, transitions are processed in the order of registration every time. Consequently, if multiple transitions apply to a specific scenario, the one that was registered first takes precedence and executes. In otherwords, once a suitable transition is found, all others are discarded; this prevents the execution of other applicable transitions since only one transition can occur per update.

### Timed transition

A "time" (or timed or unconditional) transition is triggered when a state with a max duration condition times out. If multiple "time" transitions are provided for a state, the first one registered to the FSM will be executed. (This is technically improper; see: [Time-based states](#time-based-states) for more.)

Create a "time" transition by making a `Transition` with either:

* No callbacks or trigger conditions defined; or
* A single callback function, with with no trigger conditions.

```c++
auto time_transition = Transition(StateIndex(1), StateIndex(2));

auto time_trans_w_cb = Transition(StateIndex(1), StateIndex(2),
    [](){ std::cout << "Callback fn return is void." << std::endl; }
);
```

### Transitions with Callbacks

Both conditional and timed transitions can call functions when they are triggered. Both condition and non-conditional transitions can have callbacks. Only a single callback can be associated with each transition.

### Reflexive Transitions

Reflexive transitions are those where the source and destination state is the same. They may also be called _self-transitions_. By default, there is no check for reflexive transitions when creating a `Transition`. You can prevent them by defining `FSM_ALLOW_TRANS_REFLEX` to be `0` or `false` somewhere in your code. Note that when prevented, the creation attempt will be silently ignored.

Why might you want reflexive transitions?

Both the leaving/entering callbacks for the state will execute, as well as the transition. So if you have special functionality in that manner, it will work.

Why might you want to prevent them? Well, they prevent the on-state callback from running for that state. Transitions have precedence over on-state actions, so if a transition can be taken by the model, it will.

## Actions

Actions are an additional feature of the state machine that can be linked to specific states. They provide control over a single boolean (`bool`) value so long as their associated state is active. Multiple actions can be assigned to a single state, but the reverse is not true.

The state and target of an action are assigned at `Action` creation.

### Action Types

Actions can be classified by their relationship with some external condition and by their duration with respect to the activity period of the state for which they are assigned. Time-based actions are "forced" actions, meaning that they are not passive when deactive; they will actively set their targets to `LOW`.

The following `Action` types are provided:

* `Qualifier::N` - A **Non-stored** action is an ongoing action where the variable is `True` while the associated state is active.

* `Qualifier::C` A **Conditional** action sets the variable to `True` when its associated state is active and its assigned condition is `True`.

...

### Instantaneous state transitions

Actions will not alter target values when their associated state is not active for some duration of time.

### Action effect on state exit

Actions have no effect on their target values when their associated state is no longer active; however, on state exit, actions will set their targets to `LOW` unless otherwise specified in the action description.

## Usage

The following provides examples on how to create a state machine of moderate complexity.

### Create a state

```c++
#include "finite_state_machine.h"

static void onState();  // forward declaration

// create a state with no entry/exit callbacks
spectre::State s("MyState", nullptr, onState, nullptr);
```

### Create several states using `Arduino` macros

```c++
#include "finite_state_machine.h"

enum state_alias {STATE_1, STATE_2};
const char* const stateName[] PROGMEM = {"State 1", "State 2"};

static void onState();

spectre::State s1(stateName[int(STATE_1)], nullptr, onState, nullptr);
spectre::State s2(stateName[int(STATE_2)], nullptr, onState, nullptr);
```

### Create some actions

```c++
using namespace spectre;

bool s1_is_active = false;

//...

Action act(s1.index, FSM::Qualifier::N, s1_is_active);
```

### Prepare the state machine

Since this library is designed around static memory allocation, the state machine needs to have states, transitions, and actions (S/T/As) registered to it manually. S/T/As can be assigned to multiple FSMs concurrently; this is useful in situations of parallel management.

(It is up to the user to prevent collisions and manage thread crossovers.)

```c++
//...
spectre::FSM fsm;

fsm.registerState(&s1);
fsm.registerState(&s2);

fsm.registerTransition(&s1_to_s2);

fsm.registerAction(&act);
```

### Run the state machine

```c++
#include <chrono>
#include <thread>

//...

fsm.setState(0);

uint16_t current_time;  // milliseconds

while(1)
{
    current_time = getCurrentTimeMs();
    
    // ...

    fsm.update(current_time);

    // ...
}
```
